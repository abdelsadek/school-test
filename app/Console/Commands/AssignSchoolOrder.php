<?php

namespace App\Console\Commands;

use App\Events\SchoolOrderAssigned;
use App\User;
use Illuminate\Console\Command;
use App\School;
use Event;
use App\Events\SendMail;

class AssignSchoolOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'school:order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign School order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $schools = School::with('students')->get()->sortByDesc(function($school)
        {
            return $school->students->count();
        }); //Retrieve schools ordered by students count
        $i = 0;
        foreach ($schools as $school){
            $school->update(['order' => ++$i]); //assign the new school order
        }
        $admin = User::first();
        event(new SchoolOrderAssigned($admin));
        return 1;
    }
}
