<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'school_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $createRules = [
        'name' => 'required|string',
        'school_id' => 'required|integer|exists:schools,id'
    ];
    /**
     * Validation rules
     *
     * @var array
     */
    public static $updateRules = [
        'name' => 'required|string',
        'school_id' => 'integer|exists:schools,id'
    ];
}
