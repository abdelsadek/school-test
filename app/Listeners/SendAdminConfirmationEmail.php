<?php

namespace App\Listeners;

use App\Events\SchoolOrderAssigned;
use App\Mail\AdminConfirmationEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendAdminConfirmationEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SchoolOrderAssigned  $event
     * @return void
     */
    public function handle(SchoolOrderAssigned $event)
    {
         \Mail::to($event->admin->email)->send(
            new AdminConfirmationEmail($event->admin)
        );
    }
}
