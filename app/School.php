<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Student;
class School extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'order'
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $createRules = [
        'name' => 'required|string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $updateRules = [
        'name' => 'string',
    ];


    public function students()
    {
        return $this->hasMany(Student::class, 'school_id');
    }
}
