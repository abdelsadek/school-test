<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\School;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SchoolAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),School::$createRules);
        if ($validator->fails()) {
           return $this->sendError($validator->messages(),401);
        }
        try {
            $school = School::create($request->only('name'));
            return $this->sendResponse($school,'Created Successfully');
        }catch (\Exception $ex){
            return $this->sendError($ex->getMessage(),200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),School::$updateRules);
        if ($validator->fails()) {
           return $this->sendError($validator->messages(),401);
        }
        $school = School::find($id);
        if (empty($school)) {
            return $this->sendError('Not Found');
        }

        try {
            $school->update($request->only('name'));
            return $this->sendResponse($school,'Updated Successfully');
        }catch (\Exception $ex){
            return $this->sendError($ex->getMessage(),500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /** @var School $school */
        $school = School::find($id);

        if (empty($school)) {
            return $this->sendError('Not found');
        }

        $school->delete();

        return $this->sendResponse($id,'School Deleted');
    }
}
