<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StudentAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),Student::$createRules);
        if ($validator->fails()) {
           return $this->sendError($validator->messages(),401);
        }
        try {
            $student = Student::create($request->only('name'));
            return $this->sendResponse($student,'Created Successfully');
        }catch (\Exception $ex){
            return $this->sendError($ex->getMessage(),200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),Student::$updateRules);
        if ($validator->fails()) {
           return $this->sendError($validator->messages(),401);
        }
        $student = Student::find($id);
        if (empty($student)) {
            return $this->sendError('Not Found');
        }

        try {
            $student->update($request->only('name'));
            return $this->sendResponse($student,'Updated Successfully');
        }catch (\Exception $ex){
            return $this->sendError($ex->getMessage(),500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /** @var Student $student */
        $student = Student::find($id);

        if (empty($student)) {
            return $this->sendError('Not found');
        }

        $student->delete();

        return $this->sendResponse($id,'Student Deleted');
    }
}
