<?php

namespace Tests\Feature;

use App\School;
use App\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StudentTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreate()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * A Student list test.
     *
     * @return void
     */

    public function testStudentListing()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->getToken(),
        ])->json('get', route('students.index'));
        //dd($response);
        $response->assertStatus(200);
    }
    /**
     * A Student Create test.
     *
     * @return void
     */

    public function testStudentCreate()
    {
        $school = factory(School::class)->create();
        $body = [
            'name' => 'Test Student',
            'school_id' => $school->id
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->getToken(),
        ])->post( route('students.store'),$body);
        $response->assertStatus(200);
    }
    /**
     * A Student Update test.
     *
     * @return void
     */

    public function testStudentUpdate()
    {
        $school = factory(School::class)->create();
        $student = factory(Student::class)->create(['school_id' => $school->id]);
        $body = [
            'name' => 'Test User',
            'school_id' => $school->id
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->getToken(),
        ])->put( route('students.update',[$student->id]),$body);
        $response->assertStatus(200);
    }

    /**
     * A Student Delete test.
     *
     * @return void
     */

    public function testStudentDelete()
    {
        $school = factory(School::class)->create();
        $student = factory(Student::class)->create(['school_id' => $school->id]);
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->getToken(),
        ])->delete( route('students.update',[$student->id]));
        $response->assertStatus(200);
    }

}
