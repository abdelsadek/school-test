<?php

namespace Tests\Feature;

use App\School;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SchoolTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreate()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * A School list test.
     *
     * @return void
     */

    public function testSchoolListing()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->getToken(),
        ])->json('get', route('schools.index'));
        //dd($response);
        $response->assertStatus(200);
    }
    /**
     * A School Create test.
     *
     * @return void
     */

    public function testSchoolCreate()
    {
        $body = [
            'name' => 'Test School',
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->getToken(),
        ])->post( route('schools.store'),$body);
        $response->assertStatus(200);
    }
    /**
     * A School Update test.
     *
     * @return void
     */

    public function testSchoolUpdate()
    {
        $school = factory(School::class)->create();
        $body = [
            'name' => 'Test User',
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->getToken(),
        ])->put( route('schools.update',[$school->id]),$body);
        $response->assertStatus(200);
    }

    /**
     * A School Delete test.
     *
     * @return void
     */

    public function testSchoolDelete()
    {
        $school = factory(School::class)->create();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->getToken(),
        ])->delete( route('schools.update',[$school->id]));
        $response->assertStatus(200);
    }

}
