<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SchoolsOrderTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test School order.
     *
     * @return void
     */
    public function testSchoolOrder()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->getToken(),
        ])->get( route('order-schools'));

        $response->assertStatus(200);
    }

}
