<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function getToken()
    {
       \Artisan::call("passport:install");
        $body = [
            'name' => 'Test User',
            'email' => 'test@test.com',
            'password' => '123456',
            'password_confirmation' => '123456',
        ];
         $this->post('api/signup',$body);
         $response = $this->post('api/login',$body);
         return $response['access_token'];
    }
}
