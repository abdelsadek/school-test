<?php

namespace Tests\Unit;

use App\School;
use App\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
class StudentTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCreateSchool()
    {
        $school = factory(School::class)->create();
        $student = factory(Student::class)->create(['school_id'=>$school->id]);
        $this->assertNotEmpty($student);
    }
    public function testUpdateSchool()
    {
        $school = factory(School::class)->create();
        factory(Student::class,1)->create(['school_id'=>$school->id]);
        Student::first()->update(['name'=>'Test']);
        $this->assertEquals(Student::first()->name,'Test');
    }
    public function testDeleteSchool()
    {
        $school = factory(School::class)->create();
        factory(Student::class,2)->create(['school_id'=>$school->id]);
        $id = Student::first()->id;
        Student::first()->delete();

        $this->assertEmpty(Student::find($id));
    }



}
