<?php

namespace Tests\Unit;

use App\School;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
class SchoolTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCreateSchool()
    {
        $schools = factory(School::class,2)->create();
        $this->assertCount(2,$schools);
    }
    public function testUpdateSchool()
    {
        factory(School::class,2)->create();
        School::first()->update(['name'=>'Test']);
        $this->assertEquals(School::first()->name,'Test');
    }
    public function testDeleteSchool()
    {
         factory(School::class,2)->create();
        $id = School::first()->id;
        School::first()->delete();

        $this->assertEmpty(School::find($id));
    }



}
