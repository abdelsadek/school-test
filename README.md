Setup Steps

1- Clone the repo to your server and 

2- Create .env and copy .env.example to .env then add your database and mail credentials

3- Run "composer install"

4- Run "php artisan migrate --seed"

5- Run "php artisan passport:install"

6- Run "php artisan test"
