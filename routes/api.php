<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => '\App\Http\Controllers\API'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
        Route::resource('schools','SchoolAPIController');
        Route::resource('students','StudentAPIController');
    });
    Route::get('order-schools',function (){
        Artisan::call("school:order");
        return \Response::json( [
            'success' => true,
            'data'    => [],
            'message' => "Schools orders assigned successfully",
        ],200);
    })->name('order-schools');
});

