@component('mail::message')
# Hello {{$admin->first_name}},
<br>
Schools orders assigned successfully.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
